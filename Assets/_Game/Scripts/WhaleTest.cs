﻿using UnityEngine;
using Leap;
using Leap.Unity;

public class WhaleTest : MonoBehaviour {

	public Material DefaultMaterial;
	public Material HighlightMaterial;

	void Update() {
		//bool isPointedAt = LMInputHelper.IsFingerPointingAt( transform.position, 30f, Finger.FingerType.TYPE_INDEX, Chirality.Right, true );
		bool isPointedAt = LMInputHelper.IsPointingGestureAt( transform.position, 30f, Finger.FingerType.TYPE_INDEX, Chirality.Right );

		if ( isPointedAt ) {
			GetComponent<Renderer>().material = HighlightMaterial;
		} else {
			GetComponent<Renderer>().material = DefaultMaterial;
		}

		//bool isCovering = LMInputHelper.IsFingerOverPoint( transform.position, 0.015f, Finger.FingerType.TYPE_INDEX, Chirality.Right );
		//Debug.Log( isCovering );
	}

}