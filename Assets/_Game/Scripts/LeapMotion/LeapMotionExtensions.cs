﻿namespace Leap.Unity {

	public static class LeapMotionExtensions {

		public static Finger GetFingerByType( this Hand hand, Finger.FingerType fingerType ) {
			if (hand == null)
				return null;

			for ( int f = 0; f < hand.Fingers.Count; f++ ) {
				if ( hand.Fingers[f].Type == fingerType ) {
					return hand.Fingers[f];
				}
			}

			return null;
		}

	}

}