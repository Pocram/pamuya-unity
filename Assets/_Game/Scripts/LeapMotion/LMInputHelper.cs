﻿#define LM_INPUT_HELPER_DEBUG

using UnityEngine;

namespace Leap.Unity {

	public class LMInputHelper : MonoBehaviour {

		#region Singleton

		/// <summary>
		/// Returns the instance of itself.
		/// If no instance can be found in the scene, creates one and returns it.
		/// </summary>
		private static LMInputHelper Instance {
			get {
				if (_instance == null) {
					_instance = GameObject.FindObjectOfType<LMInputHelper>();

					// No instance found? Create one!
					if (_instance == null) {
						GameObject newObject = new GameObject( "LMInputHelper", typeof (LMInputHelper) );
						_instance = newObject.GetComponent<LMInputHelper>();
					}
				}

				return _instance;
			}
		}

		private static LMInputHelper _instance;

		#endregion

		#region Exposed variables



		#endregion

		#region Private variables

		[SerializeField]
		private IHandModel _leftHandModel;

		[SerializeField]
		private IHandModel _rightHandModel;

#if LM_INPUT_HELPER_DEBUG
		private Vector2 targetScreenPosition = Vector2.zero;
		private Vector2 tipScreenPosition = Vector2.zero;
		private float tipScreenRadius = 0f;
#endif

#endregion

		#region MonoBehaviour

		private void Start() {
			//FindHandsInScene();
		}

		#endregion

		#region Public methods

		// Hand setup methods
		////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Scans the current scene for IHandModel components and 
		/// automatically assigns the first left and right hands it finds.
		/// </summary>
		public static void FindHandsInScene() {
			// Get all IHandModel components in the current scene
			IHandModel[] handModelsInScene = GameObject.FindObjectsOfType<IHandModel>();

			// Find the first left and right hands
			bool leftHandFound = false;
			bool rightHandFound = false;

			for (int i = 0; i < handModelsInScene.Length; i++) {
				if (leftHandFound == false && handModelsInScene[i].Handedness == Chirality.Left) {
					SetLeftHand( handModelsInScene[i] );
					leftHandFound = true;
					continue;
				}

				if (rightHandFound == false && handModelsInScene[i].Handedness == Chirality.Right) {
					SetRightHand( handModelsInScene[i] );
					rightHandFound = true;
					continue;
				}

				if (leftHandFound && rightHandFound)
					break;
			}
		}

		public static void SetLeftHand( IHandModel hand ) {
			Instance._leftHandModel = hand;
		}

		public static void SetRightHand( IHandModel hand ) {
			Instance._rightHandModel = hand;
		}

		// Hand detection methods
		////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Checks whether a hand is doing a pointing gesture.<para/>
		/// This method checks if only the pointing finger (and maybe the thumb) are extended. To check a single finger only, use <seealso cref="IsFingerPointingAt"/> instead.
		/// </summary>
		/// <param name="targetPosition">The targeted position in world space</param>
		/// <param name="angleThreshold">The angle at which the finger direction counts as pointing towards the target</param>
		/// <param name="requestedFinger">What kind of finger to check</param>
		/// <param name="requestedHand">Whether to check the left, right or both hands</param>
		/// <param name="allowExtendedThumb">Whether the thumb may extend or not</param>
		/// <returns></returns>
		public static bool IsPointingGestureAt( Vector3 targetPosition, float angleThreshold, Finger.FingerType requestedFinger, Chirality requestedHand, bool allowExtendedThumb = true ) {
			// Create a temporary array of both hands to be able to loop over them and reuse the code.
			IHandModel[] hands = { Instance._leftHandModel, Instance._rightHandModel };
			for ( int i = 0; i < hands.Length; i++ ) {
				if ( hands[i] == null )
					continue;

				// Skip the current hand if a specific one is requested.
				if ( requestedHand != Chirality.Either && hands[i].Handedness != requestedHand ) {
					continue;
				}

				// Get the current hand and requested finger.
				Hand currentHand = hands[i].GetLeapHand();
				Finger pointingFinger = currentHand.GetFingerByType( requestedFinger );
				if ( pointingFinger == null ) {
					continue;
				}

				// Check if the pointing finger is extended
				if ( pointingFinger.IsExtended == false ) {
					continue;
				}

				// Check if other fingers are extended
				bool isGestureValid = true;
				for (int f = 0; f < currentHand.Fingers.Count; f++) {
					// Ignore the ponting finger
					if (currentHand.Fingers[f].Type == requestedFinger) {
						continue;
					}

					// Thumb has special rules
					if (currentHand.Fingers[f].Type == Finger.FingerType.TYPE_THUMB) {
						// Only allow thumb if stated in the function call
						if (allowExtendedThumb == false && currentHand.Fingers[f].IsExtended) {
							Debug.Log( "Invalid thumb!" );
							isGestureValid = false;
							break;
						}
					} else {
						// The pointing gesture only allows the pointed finger and maybe the thumb to extend!
						if (currentHand.Fingers[f].IsExtended) {
							isGestureValid = false;
							break;
						}
					}
				}
				if (isGestureValid == false) {
					continue;
				}

				Vector3 directionToTarget = targetPosition - pointingFinger.TipPosition.ToVector3();
				Vector3 directionOfFinger = pointingFinger.Direction.ToVector3();
				float fingerToTargetAngle = Vector3.Angle( directionToTarget, directionOfFinger );

#if LM_INPUT_HELPER_DEBUG
				Color rayColour = Color.red;
				if ( fingerToTargetAngle <= angleThreshold ) {
					rayColour = Color.green;
				}

				Debug.DrawRay( pointingFinger.TipPosition.ToVector3(), directionOfFinger.normalized * 0.1f );
				Debug.DrawLine( pointingFinger.TipPosition.ToVector3(), targetPosition, rayColour );
#endif

				if ( fingerToTargetAngle <= angleThreshold ) {
					return true;
				}
			}

			// Return false if there is no hand!
			return false;
		}

		/// <summary>
		/// Checks whether a specific finger is pointing towards a position.<para/>
		/// This method checks for the finger's direction only. To check for a pointing gesture, use <seealso cref="IsPointingGestureAt"/> instead.
		/// </summary>
		/// <param name="targetPosition">The targeted position in world space</param>
		/// <param name="angleThreshold">The angle at which the finger direction counts as pointing towards the target</param>
		/// <param name="requestedFinger">What kind of finger to check</param>
		/// <param name="requestedHand">Whether to check the left, right or both hands</param>
		/// <param name="shouldFingerExtend">Whether the finger needs to extend to count as ponting</param>
		/// <returns></returns>
		public static bool IsFingerPointingAt( Vector3 targetPosition, float angleThreshold, Finger.FingerType requestedFinger, Chirality requestedHand, bool shouldFingerExtend = false ) {
			// Create a temporary array of both hands to be able to loop over them and reuse the code.
			IHandModel[] hands = { Instance._leftHandModel, Instance._rightHandModel };
			for (int i = 0; i < hands.Length; i++) {
				if (hands[i] == null)
					continue;

				// Skip the current hand if a specific one is requested.
				if (requestedHand != Chirality.Either && hands[i].Handedness != requestedHand) {
					continue;
				}

				// Get the current hand and requested finger.
				Hand currentHand = hands[i].GetLeapHand();
				Finger pointingFinger = currentHand.GetFingerByType( requestedFinger );

				if (pointingFinger == null) {
					continue;
				}

				// If needed, check if finger is extended.
				if (shouldFingerExtend && pointingFinger.IsExtended == false) {
					continue;
				}

				Vector3 directionToTarget = targetPosition - pointingFinger.TipPosition.ToVector3();
				Vector3 directionOfFinger = pointingFinger.Direction.ToVector3();
				float fingerToTargetAngle = Vector3.Angle( directionToTarget, directionOfFinger );

#if LM_INPUT_HELPER_DEBUG
				Color rayColour = Color.red;
				if ( fingerToTargetAngle <= angleThreshold ) {
					rayColour = Color.green;
				}

				Debug.DrawRay( pointingFinger.TipPosition.ToVector3(), directionOfFinger.normalized * 0.1f );
				Debug.DrawLine( pointingFinger.TipPosition.ToVector3(), targetPosition, rayColour );
#endif

				if (fingerToTargetAngle <= angleThreshold) {
					return true;
				}
			}

			// Return false if there is no hand!
			return false;
		}

		public static bool IsFingerOverPoint( Vector3 targetPoint, float fingertipRadius, Finger.FingerType requestedFinger, Chirality requestedHand ) {
			Camera mainCamera = Camera.main;
			Vector2 targetPositionOnScreen = mainCamera.WorldToScreenPoint( targetPoint );

			// Create a temporary array of both hands to be able to loop over them and reuse the code.
			IHandModel[] hands = { Instance._leftHandModel, Instance._rightHandModel };
			for ( int i = 0; i < hands.Length; i++ ) {
				if ( hands[i] == null )
					continue;

				// Skip the current hand if a specific one is requested.
				if ( requestedHand != Chirality.Either && hands[i].Handedness != requestedHand ) {
					continue;
				}

				// Get the current hand and requested finger.
				Hand currentHand = hands[i].GetLeapHand();
				Finger finger = currentHand.GetFingerByType( requestedFinger );

				if ( finger == null ) {
					continue;
				}

				// Get screen positions
				Vector2 fingertipPositionOnScreen = mainCamera.WorldToScreenPoint( finger.TipPosition.ToVector3() );

				// Get radius around tip in pixels.
				// We do that by finding two opposing points on the outter shell of an imaginary sphere around the tip of the finger.
				// These points are then transformed into screen space. Their distance is the radius in pixels.
				Vector3 spherePointA = finger.TipPosition.ToVector3() + Vector3.up * fingertipRadius;
				Vector3 spherePointB = finger.TipPosition.ToVector3() - Vector3.up * fingertipRadius;
				Vector2 spherePointAOnScreen = mainCamera.WorldToScreenPoint( spherePointA );
				Vector2 spherePointBOnScreen = mainCamera.WorldToScreenPoint( spherePointB );
				float fingertipScreenRadius = Vector2.Distance( spherePointAOnScreen, spherePointBOnScreen );

#if LM_INPUT_HELPER_DEBUG
				Debug.DrawLine( finger.TipPosition.ToVector3(), targetPoint );
				DebugExtension.DebugPoint( targetPoint, 0.5f, 0f, false );
				DebugExtension.DebugCircle( finger.TipPosition.ToVector3(), ( mainCamera.transform.position - finger.TipPosition.ToVector3() ), fingertipRadius );

				Instance.targetScreenPosition = targetPositionOnScreen;
				Instance.tipScreenPosition = fingertipPositionOnScreen;
				Instance.tipScreenRadius = fingertipScreenRadius;
#endif

				// Check if target position is within fingertip radius
				float fingertipToTargetScreenDistance = Vector2.Distance( fingertipPositionOnScreen, targetPositionOnScreen );
				if (fingertipToTargetScreenDistance <= fingertipScreenRadius ) {
#if LM_INPUT_HELPER_DEBUG
					Debug.DrawRay( finger.TipPosition.ToVector3(), Vector3.up, Color.green );
#endif
					return true;
				}
			}

			// Return false if there is no hand!
			return false;
		}

		#endregion

#if LM_INPUT_HELPER_DEBUG
		private void OnGUI() {
			string text = "Tar: " + targetScreenPosition;
			text += "\nTip: " + tipScreenPosition;
			text += "\nRad: " + tipScreenRadius;

			GUI.color = Color.black;
			GUI.Label( new Rect( 11, 11, 300, 600 ), text );
			GUI.color = Color.white;
			GUI.Label( new Rect( 10, 10, 300, 600 ), text );
		}
#endif

	}

}